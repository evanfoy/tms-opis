# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

# WARNING: Crude solution

# Initialize value:
# If value is 0, assume it is not valid. Overwrite with readback PV.
# The readback value is constrained by code and limits in the PLC.

SP_Value = PVUtil.getDouble(pvs[0])
RB_Value = PVUtil.getDouble(pvs[1])

if SP_Value == 0:
    PVUtil.writePV(pvs[1].getName(), RB_Value, 5000)
